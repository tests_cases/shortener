from sqlalchemy.orm import Session
from shortener.utils import rand_str
from sqlalchemy.exc import IntegrityError
import random

from shortener import models, schemas


def get_url(db: Session, short_url: str) -> schemas.Url:
    """Function for getting full url by its key

    Args:
        db (Session): db connection from pool
        short_url (str): key for full url

    Returns:
        schemas.Url: db row
    """
    data = db.query(models.Url).filter(models.Url.short_url == short_url).first()
    return data


def get_urls(db: Session, skip: int = 0, limit: int = 100) -> schemas.Url:
    """Get all urls

    Args:
        db (Session): db conn from pool
        skip (int, optional): how much rows to skip. Defaults to 0.
        limit (int, optional): how much rows to fetch. Defaults to 100.

    Returns:
        schemas.Url: db row
    """    
    # Limit is just for pagination purposes, which is out of scope due to notion description
    data = db.query(models.Url).offset(skip).limit(limit).all()
    return data


def get_url_stats(db: Session, short_url: str) -> schemas.Url:
    """Getting all the data about shortened url

    Args:
        db (Session): db conn from pool
        short_url (str): key for full url

    Returns:
        schemas.Url: row from db
    """    
    data = (
        db.query(models.Url)
        .with_entities(models.Url.clicks)
        .filter(models.Url.short_url == short_url)
        .first()
    )
    return data


def increment_url_clicks(db: Session, short_url: str) -> None:
    """Raising counter for url

    Args:
        db (Session): db conn from pool
        short_url (str): key for full url
    """    
    db.query(models.Url).filter(models.Url.short_url == short_url).update(
        {"clicks": models.Url.clicks + 1}
    )
    db.commit()


def create_url(db: Session, url: schemas.UrlBase) -> schemas.Url:
    """Creates url and ensures there are no collisions

    Args:
        db (Session): db conn from pool
        url (schemas.UrlBase): pydantic model for creating

    Returns:
        schemas.Url: created row from DB
    """    
    while True:
        try:
            short_url = rand_str(5)
            db_url = models.Url(short_url=short_url, full_url=url.full_url)
            db.add(db_url)
        except IntegrityError:
            print("aaaa")
        else:
            db.commit()
            db.refresh(db_url)
            break
    return db_url


def update_url(db: Session, url: schemas.UrlBase, short_url: str) -> int:
    """Updating full url by key

    Args:
        db (Session): db conn from pool
        url (schemas.UrlBase): pydantic model for imputing
        short_url (str): key for update

    Returns:
        int: number of rows affected
    """    
    cnt_upd = (
        db.query(models.Url)
        .filter(models.Url.short_url == short_url)
        .update({"full_url": url.full_url})
    )
    db.commit()
    return cnt_upd


def delete_url(db: Session, short_url: str) -> int:
    """Deleting url by key

    Args:
        db (Session): db conn from pool
        short_url (str): key for update

    Returns:
        int: number of rows affected
    """    
    cnt_del = db.query(models.Url).filter(models.Url.short_url == short_url).delete()
    db.commit()
    return cnt_del
