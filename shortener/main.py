from fastapi import Depends, FastAPI, HTTPException
from fastapi.responses import RedirectResponse
from sqlalchemy.orm import Session

from shortener import crud, models, schemas
from shortener.database import DbSession, engine

# Simplification due to project is not real. Real world project would use alembic
models.Database.metadata.create_all(bind=engine)

app = FastAPI(title="GetMatch url shortener")


def open_db_conn() -> DbSession:
    db = DbSession()
    try:
        yield db
    finally:
        db.close()


@app.post("/urls", tags=["Data modifications"], response_model=schemas.Url)
def create_url(url: schemas.UrlBase, db: Session = Depends(open_db_conn)):
    """create short url

    Args:
        url (schemas.UrlBase): url to shorten
        db (Session, optional): db session from pool. Defaults to Depends(open_db_conn).

    Returns:
        Url: created url
    """
    data = crud.create_url(db=db, url=url)
    return data


@app.put("/urls/{short_url}", tags=["Data modifications"])
def update_url(
    short_url: str, url: schemas.UrlBase, db: Session = Depends(open_db_conn)
):
    """Update short url

    Args:
        short_url (str): key to update
        url (schemas.UrlBase): body
        db (Session, optional): db conn from pool. Defaults to Depends(open_db_conn).

    Raises:
        HTTPException: Raises when no short ur found in DB
    """
    cnt_upd = crud.update_url(db=db, url=url, short_url=short_url)
    if not cnt_upd:
        raise HTTPException(
            status_code=404, detail=f"Shortened url with name {short_url} not found"
        )
    return f"Updated url {short_url}"


@app.delete("/urls/{short_url}", tags=["Data modifications"])
def delete_url(short_url: str, db: Session = Depends(open_db_conn)):
    """Delete existing url

    Args:
        short_url (str): url key to delete
        db (Session, optional): db conn from pool. Defaults to Depends(open_db_conn).

    Raises:
        HTTPException: Raises when no short ur found in DB

    Returns:
        str: number of deleted items
    """
    cnt_del = crud.delete_url(db=db, short_url=short_url)
    if not cnt_del:
        raise HTTPException(
            status_code=404, detail=f"Shortened url with name {short_url} not found"
        )
    return f"Deleted url {short_url}"


@app.get("/urls/{short_url}", tags=["Data reading"])
def redirect_to_target(short_url: str, db: Session = Depends(open_db_conn)):
    """Redeirects to target url

    Args:
        short_url (str): short url key
        db (Session, optional): db conn from pool. Defaults to Depends(open_db_conn).

    Raises:
        HTTPException: raises when no url found
    """
    data = crud.get_url(db=db, short_url=short_url)
    if not data:
        raise HTTPException(
            status_code=404, detail=f"Shortened url with name {short_url} not found"
        )
    crud.increment_url_clicks(db=db, short_url=short_url)
    return RedirectResponse(data.full_url)


@app.get(
    "/urls/{short_url}/stats", tags=["Data reading"], response_model=schemas.UrlStats
)
def get_url_stats(short_url: str, db: Session = Depends(open_db_conn)):
    """Getting stats for url

    Args:
        short_url (str): key to search stats for
        db (Session, optional): db conn from pool. Defaults to Depends(open_db_conn).

    Raises:
        HTTPException: raised when no url found

    Returns:
        schemas.UrlStats: url statistics
    """
    data = crud.get_url_stats(db=db, short_url=short_url)
    if not data:
        raise HTTPException(
            status_code=404, detail=f"Shortened url with name {short_url} not found"
        )
    return data


@app.get("/urls/", tags=["Data reading"], response_model=list[schemas.Url])
def get_all_urls(skip: int = 0, limit: int = 100, db: Session = Depends(open_db_conn)):
    """Getting all existing urls

    Args:
        skip (int, optional): rows to skip for pagination. Defaults to 0.
        limit (int, optional): rows to fetch. Defaults to 100.
        db (Session, optional): db conn from pool. Defaults to Depends(open_db_conn).

    Returns:
        list[schemas.Url]: list of existing urls
    """
    urls = crud.get_urls(db, skip=skip, limit=limit)
    return urls
