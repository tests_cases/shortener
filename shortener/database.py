from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

DB_URL = "sqlite:///./shortener_db.db"

engine = create_engine(
    DB_URL,
    connect_args={
        "check_same_thread": False
    },  # This option used for correct work with FastApi threads
)
DbSession = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Database = declarative_base()
