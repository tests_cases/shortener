import random
import string

def rand_str(length: int = 10):
    letters = string.ascii_lowercase + string.ascii_uppercase + string.digits
    rand_letters = [random.choice(letters) for _ in range(length)]
    rand_str = ''.join(rand_letters)
    return rand_str
