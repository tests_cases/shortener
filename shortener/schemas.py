from typing import Union
from pydantic import BaseModel


class UrlBase(BaseModel):
    full_url: str


class UrlStats(BaseModel):
    clicks: int


class Url(UrlBase):
    clicks: int = 0
    short_url: str

    class Config:
        orm_mode = True
