from email.policy import default
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from .database import Database


class Url(Database):
    __tablename__ = "urls"

    short_url = Column(String, primary_key=True, index=True)
    full_url = Column(String, index=True)
    clicks = Column(Integer, default=0)
