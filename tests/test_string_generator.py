from shortener.utils import rand_str
import string


def test_rand_str():
    """Testing short_url generator"""
    length = 10
    generated_str = rand_str(length=length)
    available_chars = string.ascii_lowercase + string.ascii_uppercase + string.digits

    # Make sure length is correct and chars could be used in url
    assert len(generated_str) == length
    assert all(char in available_chars for char in list(generated_str))
