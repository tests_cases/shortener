from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from shortener.database import Database
from shortener.main import app, open_db_conn

import pytest

DEFAULT_TEST_URL = "https://pornhub.com"
TEST_DB_URL = "sqlite:///./test_shortener.db"

engine = create_engine(TEST_DB_URL, connect_args={"check_same_thread": False})
TestDbSession = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Database.metadata.create_all(bind=engine)


def mock_open_db_conn():
    """Dummy DB conn creator"""
    try:
        db = TestDbSession()
        yield db
    finally:
        db.close()


app.dependency_overrides[open_db_conn] = mock_open_db_conn
client = TestClient(app)


def test_create_url():
    """Testing url creation process"""
    response = client.post(
        "/urls",
        json={"full_url": DEFAULT_TEST_URL},
    )
    data = response.json()

    # Make sure all the parameters written with correct default values
    assert response.status_code == 200
    assert "short_url" in data
    assert "clicks" in data
    assert data["full_url"] == DEFAULT_TEST_URL
    assert data["clicks"] == 0


@pytest.fixture(scope="package")
def test_url() -> str:
    """Fixture to avoid creating new urls by getting one key 
    for the whole test session

    Returns:
        str: key of added url
    """
    response = client.post(
        "/urls",
        json={"full_url": DEFAULT_TEST_URL},
    )
    shortened_url = response.json()["short_url"]
    return shortened_url


def test_redirect_to_target(test_url):
    """Testing redirects

    Args:
        test_url (fixture): pre-evaluated short url
    """    
    response = client.get(f"/urls/{test_url}", allow_redirects=False)
    assert response.status_code == 307
    assert response.headers["location"] == "https://pornhub.com"


def test_get_url_stats(test_url):
    """Testing url stats endpoint

    Args:
        test_url (fixture): pre-evaluated short url
    """    
    for _ in range(2):
        client.get(f"/urls/{test_url}", allow_redirects=False)

    # Make sure url works and forward correctly
    response = client.get(f"/urls/{test_url}/stats")
    assert response.status_code == 200
    assert response.json()["clicks"] == 3


def test_update_url(test_url):
    """Testing url update

    Args:
        test_url (fixture): pre-evaluated short url
    """    
    new_url = "https://test.com"
    response = client.put(f"/urls/{test_url}", json={"full_url": new_url})
    assert response.status_code == 200

    # Make sure redirect still works and forward correctly
    response = client.get(f"/urls/{test_url}", allow_redirects=False)
    assert response.status_code == 307
    assert response.headers["location"] == new_url

    # Make sure we didn't affect the counter
    response = client.get(f"/urls/{test_url}/stats")
    assert response.status_code == 200
    assert response.json()["clicks"] == 4


def test_delete_url(test_url):
    """Testing delete endpoint

    Args:
        test_url (fixture): pre-evaluated short url
    """    
    response = client.delete(f"/urls/{test_url}")
    assert response.status_code == 200

    # Make sure url dropped
    response = client.get(f"/urls/{test_url}", allow_redirects=False)
    assert response.status_code == 404
