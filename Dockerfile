# Alpine version wasn't chosen due to simplification of poetry install
FROM python:3.9
RUN pip install poetry

# Copying only requirements to cache them in docker layer
WORKDIR /code
COPY poetry.lock pyproject.toml /code/

# Project initialization:
RUN poetry install --no-interaction --no-ansi

# Creating folders, and files for a project:
COPY . /code
EXPOSE 8000
CMD ["poetry", "run", "uvicorn", "shortener.main:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "8000"]

