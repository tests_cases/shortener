# GetMatch url shortener
### How to run project
The easiest way (recommended) to run project is to use docker. To do this run the following command:
```sh
sh run_service.sh
```

For local run, testing and dev purposes clone this repo, make sure you have poetry installed, and run in your terminal:
```sh
poetry install
```

Then go to http://localhost:8000/docs to see swagger and play with api 🎉

### Please, keep in mind
The project supposed to be used just for demonstration purposes and assumes a lot of simplifications, such as
- only "vital" tests are written
- some rude workarounds used sometimes to simplify stack (for example random strings generator and collision checks are quite dummy)
- there are no caching mechanisms at all
- there are no persistant storage except for sqlite
- there are no migration tools
- there are no logging
- there are no environment managing
- etc